package com.example.tamagochi1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class AdapterCompra extends ArrayAdapter<Mt> {

    private Context contexto;

    public AdapterCompra(@NonNull Context context,  ArrayList<Mt> mt) {
        super(context, 0, mt);
        contexto = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Mt mt = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.design_buy, parent, false);
        }

        String nombre = mt.getNombre();
        TextView name = convertView.findViewById(R.id.comprar_nombre);
        name.setText(nombre);

        TextView potencia = convertView.findViewById(R.id.comprar_potencia);
        potencia.setText("Potencia: " + mt.getPotencia());

        TextView precision = convertView.findViewById(R.id.comprar_precision);
        precision.setText("Potencia: " + mt.getPrecision());

        TextView precio = convertView.findViewById(R.id.comprar_precio);
        precio.setText("" + mt.getPrecio());

        return convertView;
    }

}