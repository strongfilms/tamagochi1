package com.example.tamagochi1;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.net.CookieManager;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class Combates extends AppCompatActivity {

    private ProgressBar bar_me, bar_you;
    private int bucle = 1;

    private String who_am_i;

    private ImageView atacar, capturar, flee, me, you;
    private TextView message;

    private Salvaje rival;
    public float bufo = 1;
    private boolean activate_buttons = false;
    private boolean pokemonDefeated;
    String rivalCompleteName;

    static ImageView icon;
    String im;
    static ImageView lanzamiento_pokeball;
    public boolean justThrowBall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_combates);

        pokemonDefeated = false;
        icon = findViewById(R.id.attack_icon);
        lanzamiento_pokeball = findViewById(R.id.lanzamiento_pokeball);

        ArrayList<Movimiento> movs = new ArrayList<>();
        ArrayList <Salvaje> salvajes = new ArrayList<>();

        movs.add(new Movimiento("Arañazo", 40, 100));
        movs.add(new Movimiento("Ascuas", 40, 100));
        movs.add(new Movimiento("Garra Metal", 50, 95));
        movs.add(new Movimiento("Placaje", 40, 100));
        movs.add(new Movimiento("Látigo", 0, 100));
        movs.add(new Movimiento("Ataque rápido", 40, 100));
        movs.add(new Movimiento("Chispa", 65, 100));
        movs.add(new Movimiento("Teletransporte", 0, 100)); //7
        movs.add(new Movimiento("Picotazo Ven", 35, 100)); //8
        movs.add(new Movimiento("Puntapié", 65, 100)); //9
        movs.add(new Movimiento("Patada Baja", 50, 100)); //10
        movs.add(new Movimiento("Pistola Agua", 40, 100)); //11
        movs.add(new Movimiento("Onda Voltio", 60, 100)); //12
        movs.add(new Movimiento("Impactrueno", 40, 100)); //13
        movs.add(new Movimiento("Cabeza de Hierro", 80, 100)); //14
        movs.add(new Movimiento("Puño bala", 40, 100)); //15

        ArrayList<Movimiento> rattataMovs = new ArrayList<>();
        rattataMovs.add(movs.get(0));
        rattataMovs.add(movs.get(4));
        rattataMovs.add(movs.get(5));

        ArrayList<Movimiento> pidgeyMovs = new ArrayList<>();
        pidgeyMovs.add(movs.get(3));
        pidgeyMovs.add(movs.get(5));

        ArrayList<Movimiento> shinxMovs = new ArrayList<>();
        shinxMovs.add(movs.get(3));
        shinxMovs.add(movs.get(6));

        ArrayList<Movimiento> abraMovs = new ArrayList<>();
        abraMovs.add(movs.get(7));

        ArrayList<Movimiento> wurmpleMovs = new ArrayList<>();
        wurmpleMovs.add(movs.get(8));
        wurmpleMovs.add(movs.get(3));

        ArrayList<Movimiento> machopMovs = new ArrayList<>();
        machopMovs.add(movs.get(9));
        machopMovs.add(movs.get(10));

        ArrayList<Movimiento> buizelMovs = new ArrayList<>();
        buizelMovs.add(movs.get(5));
        buizelMovs.add(movs.get(11));

        ArrayList<Movimiento> electabuzzMovs = new ArrayList<>();
        electabuzzMovs.add(movs.get(12));
        electabuzzMovs.add(movs.get(13));
        electabuzzMovs.add(movs.get(5));

        ArrayList<Movimiento> scizorMovs = new ArrayList<>();
        scizorMovs.add(movs.get(14));
        scizorMovs.add(movs.get(15));

        salvajes.add(new Salvaje("rattata", rattataMovs));
        salvajes.add(new Salvaje("pidgey", pidgeyMovs));
        salvajes.add(new Salvaje("shinx", shinxMovs));
        salvajes.add(new Salvaje("abra", abraMovs));
        salvajes.add(new Salvaje("wurmple", wurmpleMovs));
        salvajes.add(new Salvaje("machop", machopMovs));
        salvajes.add(new Salvaje("buizel", buizelMovs));
        salvajes.add(new Salvaje("electabuzz", electabuzzMovs));
        salvajes.add(new Salvaje("scizor", scizorMovs));

        Random r = new Random();
        int low = 0;
        int high = salvajes.size();
        int result = r.nextInt(high-low) + low;

        //int result = 8;

        rival = salvajes.get(result);

        String first = rival.getNombre().substring(0, 1);
        String rem = rival.getNombre().substring(1);
        first = first.toUpperCase();

        rivalCompleteName = first + rem;

        first = MainActivity.im.substring(0,1);
        rem = MainActivity.im.substring(1);
        first = first.toUpperCase();
        im = first + rem;

        me = findViewById(R.id.combat_me);
        you = findViewById(R.id.combat_you);

        flee = findViewById(R.id.fleeBtn);

        bar_me = findViewById(R.id.bar_me);
        bar_you = findViewById(R.id.bar_you);

        atacar = findViewById(R.id.atacar);
        capturar = findViewById(R.id.capturar);

        message = findViewById(R.id.combat_message);

        justThrowBall = false;

        SharedPreferences sp = MainActivity.preferences;

        who_am_i = sp.getString("who_am_i", "charmander");

        setText("¡Ha aparecido un " + rivalCompleteName + " salvaje!", true);

        switch (who_am_i) {
            case "charmander":
                Glide.with(Combates.this).asGif().load(R.drawable.combat_charmander_walking).into(me);
                break;
            case "charmeleon":
                Glide.with(Combates.this).asGif().load(R.drawable.combat_charmeleon_walking).into(me);
                break;
            case "charizard":
                Glide.with(Combates.this).asGif().load(R.drawable.combat_charizard_walking).into(me);
                break;
        }

        switch (salvajes.get(result).getNombre()){
            case "rattata":
                Glide.with(Combates.this).asGif().load(R.drawable.combat_ratata_idle).into(you);
                break;
            case "pidgey":
                Glide.with(Combates.this).asGif().load(R.drawable.combat_pidgey_idle).into(you);
                break;
            case "shinx":
                Glide.with(Combates.this).asGif().load(R.drawable.combat_shinx_idle).into(you);
                break;
            case "abra":
                Glide.with(Combates.this).asGif().load(R.drawable.combat_abra_idle).into(you);
                break;
            case "wurmple":
                Glide.with(Combates.this).asGif().load(R.drawable.combat_wurmple).into(you);
                break;
            case "machop":
                Glide.with(Combates.this).asGif().load(R.drawable.combat_machop).into(you);
                break;
            case "buizel":
                Glide.with(Combates.this).asGif().load(R.drawable.combat_buizel).into(you);
                break;
            case "electabuzz":
                Glide.with(Combates.this).asGif().load(R.drawable.electabuzz_idle).into(you);
                break;
            case "scizor":
                Glide.with(Combates.this).asGif().load(R.drawable.scizor_idle).into(you);
                break;
        }

        bar_me.setProgress(100,true);
        bar_you.setProgress(100, true);

        bar_me.setProgressDrawable(getResources().getDrawable(R.drawable.combat_green));
        bar_you.setProgressDrawable(getResources().getDrawable(R.drawable.combat_green));

        //int color = 0xFF00FF00;
        //bar_me.setProgressDrawable(getResources().getDrawable(R.drawable.customprogressbar_energy));

        Handler handler = new Handler();

        atacar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Combates.this, Listado_ataques.class);
                startActivityForResult(i, 1);
            }
        });

        capturar.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.Q)
            @Override
            public void onClick(View v) {
                justThrowBall = true;
                capturar.setColorFilter(R.color.dark_red);
                handler.postDelayed(new Runnable() {
                    public void run() {
                        capturar.clearColorFilter();
                    }
                }, 75);
                lanzamiento_pokeball.bringToFront();
                lanzamiento_pokeball.setForceDarkAllowed(false);
                calculateCapture();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        you.setVisibility(View.INVISIBLE);
                    }
                }, 2350);
            }
        });

        flee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Random r = new Random();
                int low = 0;
                int high = 10;
                int result = r.nextInt(high-low) + low;

                Handler handler = new Handler();

                if(result > 3) {
                    deactivateButtons();
                    setText("Escapaste con éxito.", false);
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            finish();
                        }
                    }, 2000);
                } else {
                    deactivateButtons();
                    setText("¡No puedes escapar!", false);
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            rivalAttack(null);
                        }
                    }, 1000);
                }

            }
        });

    }

    public void changeMyBar(int progress){
        if (progress > 0) {
            bar_me.setProgress(progress, true);
            if (bar_me.getProgress() < 50 && bar_me.getProgress() > 15) {
                bar_me.setProgressDrawable(getResources().getDrawable(R.drawable.combat_yellow));
            } else if (bar_me.getProgress() <= 15){
                bar_me.setProgressDrawable(getResources().getDrawable(R.drawable.combat_red));
            }
        }else {
            bar_me.setProgress(0, true);
        }
    }

    public void changeYourBar(int progress){
        if (progress > 0) {
            bar_you.setProgress(progress, true);
            if (bar_you.getProgress() < 50 && bar_you.getProgress() > 15) {
                bar_you.setProgressDrawable(getResources().getDrawable(R.drawable.combat_yellow));
            } else if (bar_you.getProgress() <= 15){
                bar_you.setProgressDrawable(getResources().getDrawable(R.drawable.combat_red));
            }
        }else {
            bar_you.setProgress(0, true);
        }
    }

    public int calcularDano(int potencia, boolean yo){
        Random r = new Random();
        float varianza = potencia * 20 / 100;
        int result = 0;
        im = MainActivity.preferences.getString("who_am_i", "charmander");
        if (rival.getNombre() == "abra" && potencia == 0 ){
        }else{
            result = r.nextInt((int)(potencia+varianza)-(int)(potencia-varianza)) + (int)(potencia-varianza);
        }
        switch (rival.getNombre()){
            case "rattata":
            case "pidgey":
                if(yo){
                    result = result * 1;
                }else{
                    switch (im){
                        case "charmander":
                            result = (int)(result * 0.9);
                            break;
                        case "charmeleon":
                            result = (int)(result * 0.7);
                            break;
                        case "charizard":
                            result = (int)(result * 0.5);
                            break;
                    }
                }
                break;

            case "shinx":
                if (yo){
                    result = (int)(result * 0.9);
                }else{
                    switch (im){
                        case "charmander":
                            result = (int)(result * 1);
                            break;
                        case "charmeleon":
                            result = (int)(result * 0.8);
                            break;
                        case "charizard":
                            result = (int)(result * 0.6);
                            break;
                    }
                }
                break;

            case "abra":
            case "wurmple":
                if (yo){
                    result = (int)(result * 1.5);
                }else{
                    switch (im){
                        case "charmander":
                            result = (int)(result * 0.9);
                            break;
                        case "charmeleon":
                            result = (int)(result * 0.7);
                            break;
                        case "charizard":
                            result = (int)(result * 0.5);
                            break;
                    }
                }
                break;

            case "machop":
                if (yo){
                    result = (int)(result * 0.9);
                }else{
                    switch (im){
                        case "charmander":
                            result = (int)(result * 1.3);
                            break;
                        case "charmeleon":
                            result = (int)(result * 1.1);
                            break;
                        case "charizard":
                            result = (int)(result * 0.9);
                            break;
                    }
                }
                break;

            case "buizel":
                if (yo){
                    result = (int)(result * 1);
                }else{
                    switch (im){
                        case "charmander":
                            result = (int)(result * 1.2);
                            break;
                        case "charmeleon":
                            result = (int)(result * 1);
                            break;
                        case "charizard":
                            result = (int)(result * 0.8);
                            break;
                    }
                }
                break;

            case "electabuzz":
            case "scizor":
                if (yo){
                    result = (int)(result * 0.8);
                }else{
                    switch (im){
                        case "charmander":
                            result = (int)(result * 1.5);
                            break;
                        case "charmeleon":
                            result = (int)(result * 1.3);
                            break;
                        case "charizard":
                            result = (int)(result * 1);
                            break;
                    }
                }
                break;
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1){
            deactivateButtons();
            if(bar_you.getProgress() > 0){
                rivalAttack(data);
            }
        }
    }

    public void setText(String text, boolean activate) {
        activate_buttons = false;
        Thread thread = new Thread() {
            int i;
            @Override
            public void run() {
                try {
                    for (i = 0; i < text.length(); i++) { // use your variable text.leght()
                        Thread.sleep(10);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                message.setText(text.substring(0, i));
                            }
                        });
                    }
                    activate_buttons = activate;
                } catch (InterruptedException e) {
                }
            }
        };
        thread.start();

        if(activate) {
            try {
                //thread.join();
            }catch (Exception e){
            }
            activateButtons();
        }

    }

    public void myAttack(Intent data, int delay, boolean activate, boolean second){
        int danoMio = 0;
        try {
            int potenciaMio = data.getIntExtra("potencia", 0);
            danoMio = calcularDano(potenciaMio, true);
        }catch (Exception e){
        }
        atacar.setClickable(false);
        capturar.setClickable(false);
        flee.setClickable(false);

        Handler handler = new Handler();

        if(second){
            handler.postDelayed(new Runnable() {
                public void run() {
                    switch (who_am_i) {
                        case "charmander":
                            Glide.with(Combates.this).asGif().load(R.drawable.combat_charmander_atacking).into(me);
                            break;
                        case "charmeleon":
                            Glide.with(Combates.this).asGif().load(R.drawable.charmeleon_atacking).into(me);
                            break;
                        case "charizard":
                            Glide.with(Combates.this).asGif().load(R.drawable.combat_charizard_atacking).into(me);
                            break;
                    }
                }
            }, 2000);
            handler.postDelayed(new Runnable() {
                public void run() {
                    switch (who_am_i) {
                        case "charmander":
                            Glide.with(Combates.this).asGif().load(R.drawable.combat_charmander_walking).into(me);
                            break;
                        case "charmeleon":
                            Glide.with(Combates.this).asGif().load(R.drawable.combat_charmeleon_walking).into(me);
                            break;
                        case "charizard":
                            Glide.with(Combates.this).asGif().load(R.drawable.combat_charizard_walking).into(me);
                            break;
                    }
                }
            }, 3000);
        } else{
            switch (who_am_i) {
                case "charmander":
                    Glide.with(Combates.this).asGif().load(R.drawable.combat_charmander_atacking).into(me);
                    break;
                case "charmeleon":
                    Glide.with(Combates.this).asGif().load(R.drawable.charmeleon_atacking).into(me);
                    break;
                case "charizard":
                    Glide.with(Combates.this).asGif().load(R.drawable.combat_charizard_atacking).into(me);
                    break;
            }
        }
        handler.postDelayed(new Runnable() {
            public void run() {
                switch (who_am_i) {
                    case "charmander":
                        Glide.with(Combates.this).asGif().load(R.drawable.combat_charmander_walking).into(me);
                        break;
                    case "charmeleon":
                        Glide.with(Combates.this).asGif().load(R.drawable.combat_charmeleon_walking).into(me);
                        break;
                    case "charizard":
                        Glide.with(Combates.this).asGif().load(R.drawable.combat_charizard_walking).into(me);
                        break;
                }
            }
        }, 500);

        if(bar_you.getProgress()-danoMio <= 0){
            pokemonDefeated = true;
        }

        int finalDanoMio = danoMio;
        handler.postDelayed(new Runnable() {
            public void run() {
                changeYourBar(bar_you.getProgress()- finalDanoMio);
                try{
                    String first = MainActivity.im.substring(0,1);
                    String rem = MainActivity.im.substring(1);
                    first = first.toUpperCase();
                    if (bar_you.getProgress() == 0){
                        setText(first + rem + " usó " + data.getStringExtra("nombre") + "\n" + rivalCompleteName + " perdió " + finalDanoMio + " PS.\nHas ganado!", false);
                        giveExperience(rival.getNombre());
                        setResult(RESULT_OK);
                    } else{
                        setText(first + rem + " usó " + data.getStringExtra("nombre") + "\n" + rivalCompleteName + " perdió " + finalDanoMio + " PS.", activate);
                    }
                }catch (Exception e){
                }
            }
        }, delay);
    }

    public void rivalAttack(Intent data){
        Handler handler = new Handler();
        int numMovs = rival.getMovimientos().size();
        Random r = new Random();
        int low = 0;
        int movChosen = r.nextInt(numMovs-low) + low;

        int potenciaYou = rival.getMovimientos().get(movChosen).getPotencia();
        String nombreMov = rival.getMovimientos().get(movChosen).getNombre();

        if(!nombreMov.equalsIgnoreCase("Látigo")) { //Si el ataque no es Latigo

            if (!nombreMov.equalsIgnoreCase("Ataque rápido")){ //Si el ataque no es ataque rapido
                if (data != null) {
                    myAttack(data, 250, false, false); //Atacar yo, no activar botones
                }
                int danoYou = calcularDano(potenciaYou, false);
                handler.postDelayed(new Runnable() {
                    public void run() {
                        changeMyBar(bar_me.getProgress() - (int)(danoYou*bufo));
                        String first = MainActivity.im.substring(0,1);
                        String rem = MainActivity.im.substring(1);
                        first = first.toUpperCase();
                        if(bar_me.getProgress() == 0 && bar_you.getProgress() != 0){ //Ataque recibido y me debilito
                            setText(rivalCompleteName + " usó " + nombreMov + "\n" + first + rem +" perdió " + (int)(danoYou*bufo) + " PS.\nHas perdido!", false);
                            switch (who_am_i) {
                                case "charmander":
                                    Glide.with(Combates.this).asGif().load(R.drawable.combat_charmander_hurt).into(me);
                                    break;
                                case "charmeleon":
                                    Glide.with(Combates.this).asGif().load(R.drawable.combat_charmaleon_hurt).into(me);
                                    break;
                            }
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    substractHealth(20);
                                    if (MainActivity.health.getProgress() == 0){
                                        SharedPreferences.Editor editor = MainActivity.preferences.edit();
                                        editor.putBoolean("dead", true);
                                        editor.apply();
                                    }
                                    finish();
                                }
                            }, 3000);
                        }else{ //Ataque recibido pero sobrevivo
                            if (rival.getNombre() != "abra"){
                                setText(rivalCompleteName + " usó " + nombreMov + "\n" + first + rem +" perdió " + (int)(danoYou*bufo) + " PS.", true);
                                activateButtons();
                            }else{  //Teletransporte
                                deactivateButtons();
                                setText(rivalCompleteName + " usó " + nombreMov + "!", false);
                                handler.postDelayed(new Runnable() {
                                    public void run() {
                                        finish();
                                    }
                                }, 3000);
                            }
                        }
                    }
                }, 3000);
            }else{ //Si recibo ataque rapido
                int danoYou = calcularDano(potenciaYou, false);
                changeMyBar(bar_me.getProgress() - (int)(danoYou*bufo));
                deactivateButtons();
                String first = MainActivity.im.substring(0,1);
                String rem = MainActivity.im.substring(1);
                first = first.toUpperCase();
                if(bar_me.getProgress() == 0){ //Recibo y me debilito
                    setText(rivalCompleteName + " usó " + nombreMov + "\n" + first + rem + " perdió " + (int)(danoYou*bufo) + " PS.\nHas perdido!", false);
                    switch (who_am_i) {
                        case "charmander":
                            Glide.with(Combates.this).asGif().load(R.drawable.combat_charmander_hurt).into(me);
                            break;
                        case "charmeleon":
                            Glide.with(Combates.this).asGif().load(R.drawable.combat_charmaleon_hurt).into(me);
                            break;
                    }
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            substractHealth(20);
                            if (MainActivity.health.getProgress() == 0){
                                SharedPreferences.Editor editor = MainActivity.preferences.edit();
                                editor.putBoolean("dead", true);
                                editor.apply();
                            }
                            finish();
                        }
                    }, 3000);
                }else{
                    setText(rivalCompleteName + " usó " + nombreMov + "\n" + first + rem + " perdió " + (int)(danoYou*bufo) + " PS.", false); //No activo btns
                    if (!justThrowBall) {
                        myAttack(data, 2000, true, true); //activo btns
                        activateButtons();
                    }else{
                        justThrowBall = false;
                        activateButtons();
                    }

                }
            }

        } else{ //Si recibo Latigo
            deactivateButtons();
            if (data != null){
                myAttack(data, 250, false, false); //No activar btns
                if (!pokemonDefeated) {
                    bufo = (float) (bufo * 1.5);
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            setText(rivalCompleteName + " usó " + nombreMov + "!\nTu defensa ha bajado!", true);
                        }
                    }, 2000);
                }
            }else{
                if (!pokemonDefeated) {
                    bufo = (float) (bufo * 1.5);
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            setText(rivalCompleteName + " usó " + nombreMov + "!\nTu defensa ha bajado!", true);
                        }
                    }, 2000);
                }
            }
        }

    }

    public SharedPreferences.Editor createSP(){
        SharedPreferences sp = MainActivity.preferences;
        SharedPreferences.Editor editor = sp.edit();
        return editor;
    }

    public void reloadCoins(int coins){
        SharedPreferences.Editor editor = this.createSP();
        editor.putInt("coins", coins);
        editor.apply();
        MainActivity.coinCount.setText(": " + coins);
    }

    public void giveExperience(String name){
        SharedPreferences sp = MainActivity.preferences;
        SharedPreferences.Editor editor = sp.edit();
        MainActivity.lastExp = sp.getInt("exp", 33);
        switch (name) {
            case "rattata":
            case "wurmple":
                MainActivity.experiencia = MainActivity.experiencia + 3;
                MainActivity.exp.setProgress(MainActivity.experiencia);
                MainActivity.globalCoins = MainActivity.globalCoins + 2;
                reloadCoins(MainActivity.globalCoins);
                break;
            case "pidgey":
            case "shinx":
                MainActivity.experiencia = MainActivity.experiencia + 4;
                MainActivity.exp.setProgress(MainActivity.experiencia);
                MainActivity.globalCoins = MainActivity.globalCoins + 3;
                reloadCoins(MainActivity.globalCoins);
                break;
            case "abra":
                MainActivity.experiencia = MainActivity.experiencia + 2;
                MainActivity.exp.setProgress(MainActivity.experiencia);
                MainActivity.globalCoins = MainActivity.globalCoins + 1;
                reloadCoins(MainActivity.globalCoins);
                break;
            case "machop":
            case "buizel":
                MainActivity.experiencia = MainActivity.experiencia + 5;
                MainActivity.exp.setProgress(MainActivity.experiencia);
                MainActivity.globalCoins = MainActivity.globalCoins + 4;
                reloadCoins(MainActivity.globalCoins);
                break;
            case "electabuzz":
                MainActivity.experiencia = MainActivity.experiencia + 6;
                MainActivity.exp.setProgress(MainActivity.experiencia);
                MainActivity.globalCoins = MainActivity.globalCoins + 5;
                reloadCoins(MainActivity.globalCoins);
                break;
            case "scizor":
                MainActivity.experiencia = MainActivity.experiencia + 7;
                MainActivity.exp.setProgress(MainActivity.experiencia);
                MainActivity.globalCoins = MainActivity.globalCoins + 6;
                reloadCoins(MainActivity.globalCoins);
                break;
        }

        editor.putInt("exp", MainActivity.experiencia);
        if(MainActivity.experiencia >= 100){
            editor.putBoolean("should_evolve", true);
            MainActivity.shouldEvolve = true;
        }
        editor.apply();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                finish();
            }
        }, 2500);
    }

    public void substractHealth(int healthReduction){
        MainActivity.health.setProgress(MainActivity.health.getProgress() - healthReduction, true);
//        SharedPreferences.Editor editor = MainActivity.preferences.edit();
//        editor.putInt("current_health", MainActivity.health.getProgress() - healthReduction);
//        editor.apply();
    }

    public void deactivateButtons(){
        atacar.setClickable(false);
        capturar.setClickable(false);
        flee.setClickable(false);
    }

    public void activateButtons(){
        atacar.setClickable(true);
        capturar.setClickable(true);
        flee.setClickable(true);
    }

    public void calculateCapture(){

        deactivateButtons();

        Handler handler = new Handler();
        int delay = 0;

        Random r = new Random();
        int low = 0;
        int high = 5;
        int result = r.nextInt(high-low) + low;

        if (result == 1){
            Glide.with(Combates.this).asGif().load(R.drawable.capturar_capturado).into(lanzamiento_pokeball);
            delay = 7500;
        } else {
            Glide.with(Combates.this).asGif().load(R.drawable.capturar_bote1).into(lanzamiento_pokeball);
            delay = 5400;
        }

        handler.postDelayed(new Runnable() {
            public void run() {
                Glide.with(Combates.this).clear(lanzamiento_pokeball);
                if (result != 1){
                    activateButtons();
                    setText("Se ha escapado!", false);
                    rivalAttack(null);
                }
                if(result == 1){
                    Database db = new Database(getApplicationContext());
                    db.insertCaptured(rivalCompleteName);
                    db.close();
                    Intent i = new Intent(Combates.this, Galeria.class);
                    startActivity(i);
                    finish();
                }
            }
        }, delay);

        if(result != 1){
            handler.postDelayed(new Runnable() {
                public void run() {
                    you.setVisibility(View.VISIBLE);
                }
            }, delay-200);
        }

    }

}