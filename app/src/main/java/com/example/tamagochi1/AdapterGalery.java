package com.example.tamagochi1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class AdapterGalery extends ArrayAdapter<Capturado> {

    private Context contexto;

    public AdapterGalery(@NonNull Context context,  ArrayList<Capturado> pokemon) {
        super(context, 0, pokemon);
        contexto = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Capturado pokemon = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.galery_design, parent, false);
        }

        String nombre = pokemon.getNombre();
        TextView pkmnName = convertView.findViewById(R.id.galery_name);
        pkmnName.setText(nombre);

        TextView fecha = convertView.findViewById(R.id.galery_date);
        fecha.setText("Capturado el: " + pokemon.getDate());

        ImageView icono = convertView.findViewById(R.id.galeryIcon);

        switch (pokemon.getNombre()) {
            case "Rattata":
                Glide.with(contexto).asGif().load(R.drawable.combat_ratata_idle).into(icono);
                break;
            case "Pidgey":
                Glide.with(contexto).asGif().load(R.drawable.combat_pidgey_idle).into(icono);
                break;
            case "Shinx":
                Glide.with(contexto).asGif().load(R.drawable.combat_shinx_idle).into(icono);
                break;
            case "Abra":
                Glide.with(contexto).asGif().load(R.drawable.combat_abra_idle).into(icono);
                break;
            case "Wurmple":
                Glide.with(contexto).asGif().load(R.drawable.combat_wurmple).into(icono);
                break;
            case "Machop":
                Glide.with(contexto).asGif().load(R.drawable.combat_machop).into(icono);
                break;
            case "Buizel":
                Glide.with(contexto).asGif().load(R.drawable.combat_buizel).into(icono);
                break;
            case "Electabuzz":
                Glide.with(contexto).asGif().load(R.drawable.electabuzz_idle).into(icono);
                break;
            case "Scizor":
                Glide.with(contexto).asGif().load(R.drawable.scizor_idle).into(icono);
                break;
        }

        return convertView;
    }

}
