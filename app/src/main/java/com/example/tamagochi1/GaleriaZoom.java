package com.example.tamagochi1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class GaleriaZoom extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galeria_zoom);

        TextView nombre = findViewById(R.id.nombre_zoom);
        ImageView icono = findViewById(R.id.galeria_zoom);

        switch (Galeria.who_to_zoom.toLowerCase()){
            case "rattata":
                Glide.with(GaleriaZoom.this).asGif().load(R.drawable.combat_ratata_idle).into(icono);
                nombre.setText("Rattata");
                break;
            case "pidgey":
                Glide.with(GaleriaZoom.this).asGif().load(R.drawable.combat_pidgey_idle).into(icono);
                nombre.setText("Pidgey");
                break;
            case "abra":
                Glide.with(GaleriaZoom.this).asGif().load(R.drawable.combat_abra_idle).into(icono);
                nombre.setText("Abra");
                break;
            case "shinx":
                Glide.with(GaleriaZoom.this).asGif().load(R.drawable.combat_shinx_idle).into(icono);
                nombre.setText("Shinx");
                break;
            case "wurmple":
                Glide.with(GaleriaZoom.this).asGif().load(R.drawable.combat_wurmple).into(icono);
                nombre.setText("Wurmple");
                break;
            case "machop":
                Glide.with(GaleriaZoom.this).asGif().load(R.drawable.combat_machop).into(icono);
                nombre.setText("Machop");
                break;
            case "buizel":
                Glide.with(GaleriaZoom.this).asGif().load(R.drawable.combat_buizel).into(icono);
                nombre.setText("Buizel");
                break;
            case "electabuzz":
                Glide.with(GaleriaZoom.this).asGif().load(R.drawable.electabuzz_idle).into(icono);
                nombre.setText("Electabuzz");
                break;
            case "scizor":
                Glide.with(GaleriaZoom.this).asGif().load(R.drawable.scizor_idle).into(icono);
                nombre.setText("Scizor");
                break;
        }

    }
}