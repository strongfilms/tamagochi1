package com.example.tamagochi1;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class Tienda extends AppCompatActivity {

    TextView message;
    ImageView volver, comprar, vender;

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tienda);

        message = findViewById(R.id.texto_tienda);
        ImageView kecleon = findViewById(R.id.kecleon);

        comprar = findViewById(R.id.comprar);
        vender = findViewById(R.id.vender);
        volver = findViewById(R.id.tienda_volver);

        LinearLayout fondo = findViewById(R.id.fondo_tienda);
        fondo.setForceDarkAllowed(false);

        Glide.with(Tienda.this).asGif().load(R.drawable.tienda).into(kecleon);

        //setText("¡Bienvenido a la tienda Kecleon!\n¿Que desea?");

        setText("Hola~♪\nBienvenido a la tienda Kecleon~♪");

        Handler handler = new Handler();

        volver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                volver.setColorFilter(R.color.dark_red);
                handler.postDelayed(new Runnable() {
                    public void run() {
                        volver.clearColorFilter();
                    }
                }, 75);
                finish();
            }
        });

        comprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comprar.setColorFilter(R.color.dark_red);
                handler.postDelayed(new Runnable() {
                    public void run() {
                        comprar.clearColorFilter();
                        Intent i = new Intent(Tienda.this, ListaCompra.class);
                        startActivityForResult(i, 1);
                    }
                }, 75);
            }
        });

        vender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vender.setColorFilter(R.color.dark_red);
                handler.postDelayed(new Runnable() {
                    public void run() {
                        vender.clearColorFilter();
                    }
                }, 75);
            }
        });

    }

    public void setText(String text) {
        Thread thread = new Thread() {
            int i;
            @Override
            public void run() {
                try {
                    for (i = 0; i < text.length(); i++) { // use your variable text.leght()
                        Thread.sleep(10);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                message.setText(text.substring(0, i));
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        thread.start();
    }

}