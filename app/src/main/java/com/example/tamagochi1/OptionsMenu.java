package com.example.tamagochi1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

public class OptionsMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options_menu);

        ImageView galeria = findViewById(R.id.galeria);
        ImageView tienda = findViewById(R.id.tienda);

        Handler handler = new Handler();

        galeria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainActivity.health.getProgress() != 0){
                    galeria.setColorFilter(R.color.dark_red);
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            galeria.clearColorFilter();
                            Intent i = new Intent(OptionsMenu.this, Galeria.class);
                            startActivity(i);
                        }
                    }, 75);
                }
                finish();
            }
        });

        tienda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainActivity.health.getProgress() != 0){
                    tienda.setColorFilter(R.color.dark_red);
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            tienda.clearColorFilter();
                            Intent i = new Intent(OptionsMenu.this, Tienda.class);
                            startActivity(i);
                        }
                    }, 75);
                }
                finish();
            }
        });

    }
}