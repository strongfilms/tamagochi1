package com.example.tamagochi1;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListaCompra extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_compra);

        ListView list = findViewById(R.id.lista_comprar);

        TextView coins = findViewById(R.id.coinCount);

        Database db = new Database(getApplicationContext());

        if (Database.firsTime == true){
            db.createMts();
        }

        ArrayList<Mt> mt = db.readAllMts();

        ListAdapter adapter = new AdapterCompra(ListaCompra.this, mt);

        list.setAdapter(adapter);

        registerForContextMenu(list);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent data= new Intent();
                Mt mt = (Mt) parent.getItemAtPosition(position);
//                data.putExtra("potencia", mt.getPotencia());
//                data.putExtra("nombre", mt.getNombre());
//                data.putExtra("precision", mt.getPrecision());
//                data.putExtra("precio", mt.getPrecio());
//                setResult(RESULT_OK, data);
//                finish();
                AlertDialog.Builder alert = new AlertDialog.Builder(ListaCompra.this);
                alert.setCancelable(false);
                alert.setTitle("Confirmar compra");
                alert.setMessage("¿Comprar " + mt.getNombre() + " por " + mt.getPrecio() + " monedas?");

                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                alert.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (MainActivity.globalCoins >= mt.getPrecio()) {
                            Database db = new Database(getApplicationContext());
                            Movimiento mov = new Movimiento(mt.getNombre(), mt.getPotencia(), mt.getPrecision());
                            db.addMyAttack(mov);
                            MainActivity.globalCoins = MainActivity.globalCoins - mt.getPrecio();
                            reloadCoins(MainActivity.globalCoins);
                            db.removeAttack(mt.getNombre());
                            dialog.cancel();
                            finish();
                        }else{
                            dialog.cancel();
                            showInsuficientCoins();
                        }

                    }
                });

                alert.show();
            }
        });

    }

    public void reloadCoins(int coins){
        SharedPreferences sp = MainActivity.preferences;
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("coins", coins);
        editor.apply();
        MainActivity.coinCount.setText(": " + coins);
    }

    public void showInsuficientCoins(){
        AlertDialog.Builder alert = new AlertDialog.Builder(ListaCompra.this);
        alert.setCancelable(false);
        alert.setTitle("Error");
        alert.setMessage("No tienes suficientes monedas");
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alert.show();
    }

}