package com.example.tamagochi1;

import java.util.ArrayList;

public class Salvaje {

    private String nombre;
    private ArrayList<Movimiento> movimientos;

    public Salvaje() {
    }

    public Salvaje(String nombre, ArrayList<Movimiento> movimientos) {
        this.nombre = nombre;
        this.movimientos = movimientos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Movimiento> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(ArrayList<Movimiento> movimientos) {
        this.movimientos = movimientos;
    }
}
