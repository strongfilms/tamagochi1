package com.example.tamagochi1;

public class Movimiento {

    private String nombre;
    private int potencia, precision;

    public Movimiento() {
    }

    public Movimiento(String nombre, int potencia, int precision) {
        this.nombre = nombre;
        this.potencia = potencia;
        this.precision = precision;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPotencia() {
        return potencia;
    }

    public void setPotencia(int potencia) {
        this.potencia = potencia;
    }

    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
    }
}
