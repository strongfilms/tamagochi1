package com.example.tamagochi1;

public class Colores {

    private String tipo;
    private int color;

    public Colores() {
    }

    public Colores(String tipo, int color) {
        this.tipo = tipo;
        this.color = color;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
