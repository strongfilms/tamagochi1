package com.example.tamagochi1;

public class Capturado {

    private String nombre, date;

    public Capturado() {
    }

    public Capturado(String nombre, String date) {
        this.nombre = nombre;
        this.date = date;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
