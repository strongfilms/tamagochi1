package com.example.tamagochi1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Listado_ataques extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_ataques);

        ListView list = findViewById(R.id.atacks_list);

        Database db = new Database(getApplicationContext());
        db.createMyMoves();
        db.close();

        db = new Database(getApplicationContext());
        ArrayList<Movimiento> movs = db.readMyMoves();

        Log.i("logeo", "size: " + movs.size());
        ListAdapter adapter = new AdapterPers(Listado_ataques.this, movs);
        db.close();
        list.setAdapter(adapter);

        registerForContextMenu(list);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent data= new Intent();
                Movimiento mov = (Movimiento) parent.getItemAtPosition(position);
                data.putExtra("potencia", mov.getPotencia());
                data.putExtra("nombre", mov.getNombre());
                data.putExtra("precision", mov.getPrecision());
                setResult(RESULT_OK,data);
                finish();
            }
        });

    }

}