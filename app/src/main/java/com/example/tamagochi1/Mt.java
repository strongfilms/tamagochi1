package com.example.tamagochi1;

public class Mt {

    private String nombre;
    private int potencia, precision;
    private int precio;

    public Mt() {
    }

    public Mt(String nombre, int potencia, int precision, int precio) {
        this.nombre = nombre;
        this.potencia = potencia;
        this.precision = precision;
        this.precio = precio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPotencia() {
        return potencia;
    }

    public void setPotencia(int potencia) {
        this.potencia = potencia;
    }

    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }
}
