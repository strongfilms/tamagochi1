package com.example.tamagochi1;

public class Data {

    private String game_started, start_date, start_time, exit_date, exit_time, currentHealth, who_am_i;
    private int comida;

    public Data(String game_started, String start_date, String start_time, String exit_date, String exit_time, String currentHealth, String who_am_i, int comida) {
        this.game_started = game_started;
        this.start_date = start_date;
        this.start_time = start_time;
        this.exit_date = exit_date;
        this.exit_time = exit_time;
        this.currentHealth = currentHealth;
        this.who_am_i = who_am_i;
        this.comida = comida;
    }

    public Data() {
    }

    public String getGame_started() {
        return game_started;
    }

    public void setGame_started(String game_started) {
        this.game_started = game_started;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getExit_date() {
        return exit_date;
    }

    public void setExit_date(String exit_date) {
        this.exit_date = exit_date;
    }

    public String getExit_time() {
        return exit_time;
    }

    public void setExit_time(String exit_time) {
        this.exit_time = exit_time;
    }

    public String getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(String currentHealth) {
        this.currentHealth = currentHealth;
    }

    public String getWho_am_i() {
        return who_am_i;
    }

    public void setWho_am_i(String who_am_i) {
        this.who_am_i = who_am_i;
    }

    public int getComida() {
        return comida;
    }

    public void setComida(int comida) {
        this.comida = comida;
    }
}
