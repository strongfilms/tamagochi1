package com.example.tamagochi1;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class AdapterPers extends ArrayAdapter<Movimiento> {

    public AdapterPers(Context context, ArrayList<Movimiento> movimientos) {
        super(context, 0, movimientos);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Movimiento movimiento = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_design, parent, false);
        }

        String nombre = movimiento.getNombre();
        TextView nombreTv = convertView.findViewById(R.id.lista_nombre_ataque);
        nombreTv.setText(nombre);

        int potencia = movimiento.getPotencia();
        TextView potenciaTv = convertView.findViewById(R.id.lista_potencia);
        potenciaTv.setText("Potencia: " + potencia);

        int precision = movimiento.getPrecision();
        TextView precisionTv = convertView.findViewById(R.id.lista_precision);
        precisionTv.setText("Precisión: " + precision);

        ImageView icono = convertView.findViewById(R.id.attack_icon);
        LinearLayout fondo = convertView.findViewById(R.id.lista_background);

        switch (movimiento.getNombre()) {
            case "Arañazo":
                icono.setImageDrawable(MainActivity.typeIcons.get(0));
                for (Colores color : MainActivity.colores){
                    if(color.getTipo().equalsIgnoreCase("normal")){
                        fondo.setBackgroundColor(color.getColor());
                    }
                }
                break;
            case "Garra Metal":
                icono.setImageDrawable(MainActivity.typeIcons.get(2));
                for (Colores color : MainActivity.colores){
                    if(color.getTipo().equalsIgnoreCase("acero")){
                        fondo.setBackgroundColor(color.getColor());
                    }
                }
                break;
            case "Ascuas":
            case "Lanzallamas":
            case "Sofoco":
                icono.setImageDrawable(MainActivity.typeIcons.get(1));
                for (Colores color : MainActivity.colores){
                    if(color.getTipo().equalsIgnoreCase("fuego")){
                        fondo.setBackgroundColor(color.getColor());
                    }
                }
                break;
            case "Garra Dragón":
                icono.setImageDrawable(MainActivity.typeIcons.get(3));
                for (Colores color : MainActivity.colores){
                    if(color.getTipo().equalsIgnoreCase("dragon")){
                        fondo.setBackgroundColor(color.getColor());
                    }
                }
                break;
        }

        // Lookup view for data population
        //TextView nombreHilo = (TextView) convertView.findViewById(R.id.lista_nombre_ataque);
        // Populate the data into the template view using the data object
        //nombreHilo.setText(hilo.getTitulo());
        // Return the completed view to render on screen
        return convertView;
    }

}
