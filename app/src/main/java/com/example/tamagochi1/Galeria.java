package com.example.tamagochi1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Galeria extends AppCompatActivity {

    ListView lista_galeria;
    static String who_to_zoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galeria);

        lista_galeria = findViewById(R.id.lista_galeria);

        Database db = new Database(getApplicationContext());
        ArrayList<Capturado> pokemon = db.readAllCaptured();

        Log.i("logeo", "capturados: " + pokemon.size());
        ListAdapter adapter = new AdapterGalery(Galeria.this, pokemon);
        db.close();
        lista_galeria.setAdapter(adapter);

        registerForContextMenu(lista_galeria);

        lista_galeria.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(Galeria.this, GaleriaZoom.class);
                Capturado capturado = (Capturado) parent.getItemAtPosition(position);
                who_to_zoom = capturado.getNombre();
                startActivity(i);
            }
        });

    }
}