package com.example.tamagochi1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.SAXParser;

public class Database extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = "Data.db";
    public static final String TABLA_NOMBRES = "registry";
    public static final String COLUMNA_ID = "_id";
    public static final String GAME_STARTED = "game_started";
    public static final String START_DATE = "start_date";
    public static final String START_TIME = "start_time";
    public static final String EXIT_DATE = "exit_date";
    public static final String EXIT_TIME = "exit_time";
    public static final String CURRENT_HEALTH = "current_health";
    public static final String WHO_AM_I = "who_am_i";
    public static final String COMIDA = "comida";
    public static String text = null;

    public static boolean firsTime = true;

    public static final String MOVIMIENTOS = "movimientos";
    public static final String MOVIMIENTOS_NOMBRE = "nombre";
    public static final String MOVIMIENTOS_POTENCIA = "potencia";
    public static final String MOVIMIENTOS_PRECISION = "precision";

    private static final String CREAR_MOVIMIENTOS = "create table if not exists "
            + MOVIMIENTOS + "("
            + MOVIMIENTOS_NOMBRE + " text primary key not null,"
            + MOVIMIENTOS_POTENCIA + " integer not null,"
            + MOVIMIENTOS_PRECISION  + " integer not null"
            + ");";

    private static final String MIS_MOVIMIENTOS = "create table if not exists "
            + "mis_movimientos" + "("
            + MOVIMIENTOS_NOMBRE + " text primary key not null,"
            + MOVIMIENTOS_POTENCIA + " integer not null,"
            + MOVIMIENTOS_PRECISION  + " integer not null"
            + ");";

    private static final String MIS_MTS = "create table if not exists "
            + "mts" + "("
            + MOVIMIENTOS_NOMBRE + " text primary key not null,"
            + MOVIMIENTOS_POTENCIA + " integer not null,"
            + MOVIMIENTOS_PRECISION  + " integer not null,"
            + "precio" + " integer not null"
            + ");";

    private static final String CREATE_CAPTURADOS = "create table if not exists "
            + "capturados" + "("
            + "nombre" + " text primary key not null,"
            + "date" + " text not null"
            + ");";

//    private static final String SQL_CREAR = "create table "
//            + TABLA_NOMBRES + "(" + COLUMNA_ID
//            + " integer primary key autoincrement, "
//            + GAME_STARTED + " text not null,"
//            + START_DATE + " text not null,"
//            + START_TIME  + " text not null, "
//            + EXIT_DATE + " text,"
//            + EXIT_TIME + " text,"
//            + CURRENT_HEALTH + " integer,"
//            + WHO_AM_I + " text not null,"
//            + COMIDA + " integer not null"
//            + ");";

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //db.execSQL(SQL_CREAR);

        db.execSQL(CREAR_MOVIMIENTOS);
        db.execSQL(MIS_MOVIMIENTOS);
        db.execSQL(CREATE_CAPTURADOS);
        db.execSQL(MIS_MTS);
        //this.createAllMoves();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL("drop table mis_movimientos");
        //db.execSQL("drop table movimientos");
        //db.execSQL("drop table capturados");
        db.execSQL(CREAR_MOVIMIENTOS);
        db.execSQL(MIS_MOVIMIENTOS);
        db.execSQL(CREATE_CAPTURADOS);
        db.execSQL(MIS_MTS);
    }

    public Data readAll(){
        Data data = new Data();
        SQLiteDatabase db = this.getReadableDatabase();
        String[] projection = {GAME_STARTED, START_DATE, START_TIME, EXIT_DATE, EXIT_TIME, CURRENT_HEALTH, WHO_AM_I, COMIDA};
        Cursor cursor =
                db.query(TABLA_NOMBRES,
                        projection,
                        null,
                        null,
                        null,
                        null,
                        null);
        if (cursor.moveToNext()){
            data = new Data(
                    cursor.getString(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getString(6),
                    cursor.getInt(7));
        }
        return data;
    }

    public ArrayList readMyMoves() {
        ArrayList <Movimiento> movimientos = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String[] projection = {"nombre", "potencia", "precision"};
        Cursor cursor =
                db.query("mis_movimientos",
                        projection,
                        null,
                        null,
                        null,
                        null,
                        null);

        while (cursor.moveToNext()){
            Movimiento mov = new Movimiento(cursor.getString(0), cursor.getInt(1), cursor.getInt(2));
            movimientos.add(mov);
        }

        return movimientos;
    }

    public ArrayList readAllMoves() {
        ArrayList <Movimiento> movimientos = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String[] projection = {"nombre", "potencia", "precision"};
        Cursor cursor =
                db.query(MOVIMIENTOS,
                        projection,
                        null,
                        null,
                        null,
                        null,
                        null);

        while (cursor.moveToNext()){
            Movimiento mov = new Movimiento(cursor.getString(0), cursor.getInt(1), cursor.getInt(2));
            movimientos.add(mov);
        }

        return movimientos;
    }

    public ArrayList readAllCaptured() {
        ArrayList <Capturado> pokemon = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String[] projection = {"nombre", "date"};
        Cursor cursor =
                db.query("capturados",
                        projection,
                        null,
                        null,
                        null,
                        null,
                        null);

        while (cursor.moveToNext()){
            Capturado salvaje = new Capturado(cursor.getString(0), cursor.getString(1));
            pokemon.add(salvaje);
        }

        return pokemon;
    }

    public void createAllMoves(){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Movimiento> movs = new ArrayList<>();
        movs.add(new Movimiento("Arañazo", 40, 100));
        movs.add(new Movimiento("Ascuas", 40, 100));
        movs.add(new Movimiento("Garra Metal", 50, 95));
        movs.add(new Movimiento("Placaje", 40, 100));
        movs.add(new Movimiento("Látigo", 0, 100));
        movs.add(new Movimiento("Ataque rápido", 40, 100));
        movs.add(new Movimiento("Chispa", 65, 100));
        movs.add(new Movimiento("Teletransporte", 0, 100)); //7
        movs.add(new Movimiento("Picotazo Ven", 35, 100)); //8
        movs.add(new Movimiento("Puntapié", 65, 100)); //9
        movs.add(new Movimiento("Patada Baja", 50, 100)); //10
        movs.add(new Movimiento("Pistola Agua", 40, 100)); //11
        movs.add(new Movimiento("Onda Voltio", 60, 100)); //12
        movs.add(new Movimiento("Impactrueno", 40, 100)); //13
        movs.add(new Movimiento("Cabeza de Hierro", 80, 100)); //14
        movs.add(new Movimiento("Puño bala", 40, 100)); //15

        Log.i("logeo", "Hola: " + this.readAllMoves().size());

        for (Movimiento mov : movs) {
            ContentValues values = new ContentValues();
            values.put("nombre", mov.getNombre());
            values.put("potencia", mov.getPotencia());
            values.put("precision", mov.getPrecision());
            db.insert(MOVIMIENTOS, null,  values);
        }

        db.close();
    }

    public void createMyMoves(){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Movimiento> movs = new ArrayList<>();
        movs.add(new Movimiento("Arañazo", 40, 100));
        movs.add(new Movimiento("Ascuas", 40, 100));
        movs.add(new Movimiento("Garra Metal", 50, 95));
        Log.i("moviss", "1");
        for (Movimiento mov : movs) {
            ContentValues values = new ContentValues();
            values.put("nombre", mov.getNombre());
            values.put("potencia", mov.getPotencia());
            values.put("precision", mov.getPrecision());
            Log.i("moviss", "2");
            db.insert("mis_movimientos", null,  values);
            Log.i("moviss", "3");
        }

        db.close();
    }

    public void createMts(){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Mt> mt = new ArrayList<>();

        if (firsTime == true) {
            mt.add(new Mt("Lanzallamas", 95, 100, 80));
            mt.add(new Mt("Sofoco", 130, 100, 150));
            mt.add(new Mt("Garra Dragón", 80, 100, 70));
        }

        firsTime = false;

        for (Mt maquina : mt) {
            ContentValues values = new ContentValues();
            values.put("nombre", maquina.getNombre());
            values.put("potencia", maquina.getPotencia());
            values.put("precision", maquina.getPrecision());
            values.put("precio", maquina.getPrecio());
            db.insert("mts", null,  values);
        }
        db.close();
    }

    public ArrayList readAllMts() {
        ArrayList <Mt> mt = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String[] projection = {"nombre", "potencia", "precision", "precio"};
        Cursor cursor =
                db.query("mts",
                        projection,
                        null,
                        null,
                        null,
                        null,
                        null);

        while (cursor.moveToNext()){
            Mt maquina = new Mt(cursor.getString(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3));
            mt.add(maquina);
        }

        return mt;
    }

    public void insertCaptured(String nombre){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Salvaje> pokemons = new ArrayList<>();
        pokemons.add(new Salvaje(nombre, null));

        String fecha = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());

        ContentValues values = new ContentValues();
        values.put("nombre", pokemons.get(0).getNombre());
        values.put("date", fecha);
        db.insert("capturados", null,  values);

        db.close();
    }

    public void addMyAttack(Movimiento mov){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("nombre", mov.getNombre());
        values.put("potencia", mov.getPotencia());
        values.put("precision", mov.getPrecision());
        db.insert("mis_movimientos", null,  values);

        db.close();
    }

    public void removeAttack(String mov){
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete("mts", "nombre" + "='" + mov + "'", null);
    }

}
