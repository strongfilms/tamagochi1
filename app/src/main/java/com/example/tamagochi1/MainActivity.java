package com.example.tamagochi1;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    static ProgressBar energy, health, exp;
    private String currentHour, currentMinute;
    static String start_time = "";
    static String start_date = "", game_started = "";
    ImageView spritePkm, curar, combatir, more;
    static int comida = 0;
    static int experiencia = 0;
    static TextView coinCount;
    static ArrayList<Drawable> typeIcons;
    static ArrayList<Colores> colores;
    static ArrayList<Drawable> icons_salvajes;
    static String im;
    static int globalCoins;
    static SharedPreferences preferences;
    static long ultimaBajadaEnergia;
    LinearLayout health_info, energy_info;
    boolean firstTimeEver;
    static boolean shouldEvolve;
    static int lastExp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        typeIcons = new ArrayList<>();

        typeIcons.add(getDrawable(R.drawable.normal));
        typeIcons.add(getDrawable(R.drawable.fuego));
        typeIcons.add(getDrawable(R.drawable.acero));
        typeIcons.add(getDrawable(R.drawable.dragon));

        colores = new ArrayList<>();

        colores.add(new Colores("fuego", getResources().getColor(R.color.fuego)));
        colores.add(new Colores("normal", getResources().getColor(R.color.normal)));
        colores.add(new Colores("acero", getResources().getColor(R.color.acero)));
        colores.add(new Colores("dragon", getResources().getColor(R.color.dragon)));

        icons_salvajes = new ArrayList<>();

        icons_salvajes.add(getDrawable(R.drawable.combat_ratata_idle));
        icons_salvajes.add(getDrawable(R.drawable.combat_pidgey_idle));

        spritePkm = (ImageView) findViewById(R.id.pokemonSprite);

        coinCount = findViewById(R.id.coinCount);

        curar = findViewById(R.id.curar);
        combatir = findViewById(R.id.combatir);
        more = findViewById(R.id.more);

        health_info = findViewById(R.id.health_info);
        energy_info = findViewById(R.id.energy_info);

        //Button morir = findViewById(R.id.morir);
        health = findViewById(R.id.progressBar);
        energy = findViewById(R.id.energyBar);
        exp = findViewById(R.id.expBar);

        //health.setProgress(100, true);

        SharedPreferences sp = MainActivity.this.getPreferences(MODE_PRIVATE);
        MainActivity.preferences = sp;
        SharedPreferences.Editor editor = sp.edit();

        experiencia = sp.getInt("exp", 0);

        int energyNumber = sp.getInt("current_energy", 98);
        int healthNumber = sp.getInt("current_health", 98);
        energy.setProgress(energyNumber, true);
        health.setProgress(healthNumber, true);
        exp.setProgress(experiencia);

        firstTimeEver = sp.getBoolean("first_time_ever", true);
        shouldEvolve = sp.getBoolean("should_evolve", false);

        if(firstTimeEver){
            energy.setProgress(100, true);
        }

        loadAvatar();

        long defaultBajada = new Date().getTime();

        ultimaBajadaEnergia = sp.getLong("ultima_bajada", defaultBajada);
        editor.putLong("ultima_bajada", ultimaBajadaEnergia);
        //editor.putInt("exp", 94);
        editor.apply();

        Date fechaBajada = new Date(ultimaBajadaEnergia);
        //fechaBajada.setDate((int)ultimaBajadaEnergia);

        Date fechaAhora = new Date();

        //diff
        long fechaBajadaMs = fechaBajada.getTime();
        long fechaAhoraMs = fechaAhora.getTime();

        long diferencia = fechaAhoraMs - fechaBajadaMs;
        float dias = (diferencia/ (1000 * 60 * 60) / 24);
        float horas = (diferencia/ (1000 * 60 * 60));
        float minutos = (diferencia/ (1000 * 60));

        //Toast.makeText(MainActivity.this, "Fecha ultima bajada: " + fechaBajada, Toast.LENGTH_SHORT).show();
        //Toast.makeText(MainActivity.this, "Fecha ahora: " + fechaAhora, Toast.LENGTH_SHORT).show();

        //Toast.makeText(MainActivity.this, "Diferencia:\nDias: " + dias + "\nHoras: " + horas + "\nMinutos: " + minutos, Toast.LENGTH_LONG).show();

        if(!firstTimeEver){
            if(minutos >= 20 && energy.getProgress() < 100){
                int num_ciclos = (int)(minutos / 1);
                int energia = 0;
                for (int i = 0; i<num_ciclos; i++){
                    energia = energia + 30;
                }
                energy.setProgress(energy.getProgress() + energia, true);
                Date ahora = new Date();
                ultimaBajadaEnergia = ahora.getTime();
                SharedPreferences.Editor editor2 = sp.edit();
                editor2.putLong("ultima_bajada", ultimaBajadaEnergia);
                editor2.putInt("current_energy", energy.getProgress());
                editor2.apply();
            }
        }

        if ((int)(horas) >= 6) {
            int num_ciclos = (int)(horas / 6);
            int daño = 0;
            if(!firstTimeEver){
                for (int i = 0; i<num_ciclos; i++){
                    daño = daño + 18;
                }
                health.setProgress(health.getProgress() - daño, true);
                if(health.getProgress() == 0){
                    die();
                }
            }
        }

        Database db = new Database(getApplicationContext());
        try{
            db.createAllMoves();
        }catch (Exception e){
        }

        db.createMyMoves();

        im = sp.getString("who_am_i", "charmander");
        globalCoins = sp.getInt("coins", 0);
        coinCount.setText(": " + globalCoins);

        if (im.equalsIgnoreCase("charmander") && exp.getProgress() == 100){
            exp.setProgress(0, true);
            editor.putString("who_am_i", "charmeleon");
            im = "charmeleon";
            MainActivity.experiencia = 0;
            loadAvatar();
            editor.putInt("exp", 0);
            editor.apply();
        } else if(im.equalsIgnoreCase("charmeleon") && exp.getProgress() == 100){
            editor.putString("who_am_i", "charizard");
            im = "charizard";
            MainActivity.experiencia = 0;
            loadAvatar();
            editor.putInt("exp", 0);
            editor.apply();
        }

        Handler handler = new Handler();

        energy_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ultimaBajadaEnergia = sp.getLong("ultima_bajada", 0L);

                Date fechaBajada = new Date(ultimaBajadaEnergia);
                //fechaBajada.setDate((int)ultimaBajadaEnergia);
                Date fechaAhora = new Date();
                //diff
                long fechaBajadaMs = fechaBajada.getTime();
                long fechaAhoraMs = fechaAhora.getTime();

                long diferencia = fechaAhoraMs - fechaBajadaMs;
                float dias = (diferencia/ (1000 * 60 * 60) / 24);
                float horas = (diferencia/ (1000 * 60 * 60));
                float minutos = (diferencia/ (1000 * 60));

                if(energy.getProgress() != 100){
                    Toast.makeText(MainActivity.this, "Faltan " + (int)(20 - minutos) + " minutos para ganar energía",Toast.LENGTH_LONG).show();
                }

            }
        });

        curar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (health.getProgress() != 0){
                    if (health.getProgress() < 100) {
                        curar.setColorFilter(R.color.dark_red);
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                curar.clearColorFilter();
                            }
                        }, 75);
                        comer();
                    }else {
                        Toast.makeText(MainActivity.this, "La salud ya está al máximo", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        combatir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (health.getProgress() != 0){
                    combatir.setColorFilter(R.color.dark_red);
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            combatir.clearColorFilter();
                        }
                    }, 75);
                    if(energy.getProgress() >= 20) {
                        energy.setProgress(energy.getProgress()-20);
                        //long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
                        Date ahora = new Date();
                        //ultimaBajadaEnergia = ahora.getTime();
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putLong("ultima_bajada", ultimaBajadaEnergia);
                        editor.apply();
                        Intent i = new Intent(MainActivity.this, Combates.class);
                        startActivityForResult(i, 2);
                    }else{
                        Toast.makeText(MainActivity.this, "No tienes suficiente energía", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainActivity.health.getProgress() != 0){
                    more.setColorFilter(R.color.dark_red);
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            more.clearColorFilter();
                            Intent i = new Intent(MainActivity.this, OptionsMenu.class);
                            startActivity(i);
                        }
                    }, 75);
                }
            }
        });

    }

    public void die(){
        SharedPreferences sp = MainActivity.this.getPreferences(MODE_PRIVATE);
        String soy = sp.getString("who_am_i", "charmander");
        switch (soy){
            case "charmander":
                Glide.with(MainActivity.this).asGif().load(R.drawable.dead_charmander).into(spritePkm);
                break;
            case "charmeleon":
                Glide.with(MainActivity.this).asGif().load(R.drawable.dead_charmaleon).into(spritePkm);
                break;
            case "charizard":
                Glide.with(MainActivity.this).asGif().load(R.drawable.charmander_sleeping).into(spritePkm);
                break;
            default:
                Toast.makeText(MainActivity.this, "Null", Toast.LENGTH_SHORT).show();
                break;
        }
        health.setProgress(0,true);
        energy.setProgress(0, true);
        curar.setClickable(false);
        combatir.setClickable(false);
        more.setClickable(false);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("current_health", 0);
        editor.putInt("current_energy", 0);
        editor.putBoolean("dead", true);
        editor.apply();
    }

    public void comer(){
        if(health.getProgress() != 0){
            SharedPreferences sp = MainActivity.this.getPreferences(MODE_PRIVATE);
            String soy = sp.getString("who_am_i", "charmander");
            String hourPicker = new SimpleDateFormat("HH", Locale.getDefault()).format(new Date());
            int hour = Integer.parseInt(hourPicker);
            comida = 10;
            if (comida > 1) {
                switch (soy){
                    default:
                    case "charmander":
                        Glide.with(MainActivity.this).asGif().load(R.drawable.charmander_eating).into(spritePkm);
                        break;
                    case "charmeleon":
                        Glide.with(MainActivity.this).asGif().load(R.drawable.charmeleon_eating).into(spritePkm);
                        break;
                    case "charizard":
                        Glide.with(MainActivity.this).asGif().load(R.drawable.charizard_eating).into(spritePkm);
                        break;
                }
                comida--;
                Database db = new Database(getApplicationContext());
                //int co = db.changeFoodCount();

                //Toast.makeText(MainActivity.this, "Comida: " + co, Toast.LENGTH_SHORT).show();
                curar.setClickable(false);
                combatir.setClickable(false);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        switch (soy) {

                            case "charmander":
                                if (hour > 22 || hour < 8) {
                                    Glide.with(MainActivity.this).asGif().load(R.drawable.charmander_sleeping).into(spritePkm);
                                } else {
                                    Glide.with(MainActivity.this).asGif().load(R.drawable.charmander_idle).into(spritePkm);
                                }
                                break;

                            case "charmeleon":
                                if (hour > 22 || hour < 8) {
                                    Glide.with(MainActivity.this).asGif().load(R.drawable.sleeping_charmeleon).into(spritePkm);
                                } else {
                                    Glide.with(MainActivity.this).asGif().load(R.drawable.charmeleon_idle).into(spritePkm);
                                }
                                break;

                            case "charizard":
                                if (hour > 22 || hour < 8) {
                                    Glide.with(MainActivity.this).asGif().load(R.drawable.charizard_sleeping).into(spritePkm);
                                } else {
                                    Glide.with(MainActivity.this).asGif().load(R.drawable.charizard_idle).into(spritePkm);
                                }
                                break;

                        }
                        health.setProgress(health.getProgress() + 20, true);
                        curar.setClickable(true);
                        combatir.setClickable(true);
                    }
                }, 3000);
            }else{
                Toast.makeText(MainActivity.this, "No queda comida!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onPause() {
        //Toast.makeText(MainActivity.this, currentHour, Toast.LENGTH_SHORT).show();
        //writeDatabase();
        SharedPreferences sp = MainActivity.this.getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        int currentHealth = health.getProgress();
        int currentEnergy = energy.getProgress();

        String whoAmI = sp.getString("who_am_i", "charmander");

        String text = (String) coinCount.getText();
        int coins = Integer.parseInt(text.substring(2));

        editor.putInt("coins", coins);
        editor.putBoolean("first_time_ever", false);
        editor.putInt("current_health", currentHealth);
        editor.putInt("current_energy", currentEnergy);
        editor.putString("who_am_i", whoAmI);
        editor.putInt("exp", this.experiencia);
        editor.apply();
        super.onPause();
    }

    public void loadAvatar(){
        //Database db = new Database(getApplicationContext());
        //Data data = db.readAll();
        String hourPicker = new SimpleDateFormat("HH", Locale.getDefault()).format(new Date());
        int hour = Integer.parseInt(hourPicker);
        SharedPreferences sp = MainActivity.this.getPreferences(MODE_PRIVATE);
        String soy = sp.getString("who_am_i", "charmander");
        boolean dead =  sp.getBoolean("dead", false);
        //Toast.makeText(MainActivity.this, "Soy: " + soy, Toast.LENGTH_SHORT).show();
        switch (soy){
            case "charmander":
                if(!dead){
                    if(hour > 22 || hour < 8){
                        Glide.with(MainActivity.this).asGif().load(R.drawable.charmander_sleeping).into(spritePkm);
                    }else{
                        Glide.with(MainActivity.this).asGif().load(R.drawable.charmander_idle).into(spritePkm);
                    }
                }else{
                    Glide.with(MainActivity.this).asGif().load(R.drawable.dead_charmander).into(spritePkm);
                }
                break;
            case "charmeleon":
                if(!dead){
                    if(hour > 22 || hour < 8){
                        Glide.with(MainActivity.this).asGif().load(R.drawable.sleeping_charmeleon).into(spritePkm);
                    }else{
                        Glide.with(MainActivity.this).asGif().load(R.drawable.charmeleon_idle).into(spritePkm);
                    }
                }else{
                    Glide.with(MainActivity.this).asGif().load(R.drawable.dead_charmaleon).into(spritePkm);
                }
                break;
            case "charizard":
                if(!dead){
                    if(hour > 22 || hour < 8){
                        Glide.with(MainActivity.this).asGif().load(R.drawable.charizard_sleeping).into(spritePkm);
                    }else{
                        Glide.with(MainActivity.this).asGif().load(R.drawable.charizard_idle).into(spritePkm);
                    }
                }else{
                    Glide.with(MainActivity.this).asGif().load(R.drawable.dead_charizard).into(spritePkm);
                }
                break;
            case "grave":
                Glide.with(MainActivity.this).asGif().load(R.drawable.dead_charmander).into(spritePkm);
                break;
            default:
                Toast.makeText(MainActivity.this, "Null", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 2){
//            SharedPreferences sp = MainActivity.this.getPreferences(MODE_PRIVATE);
//            SharedPreferences.Editor editor = sp.edit();
//
//            editor.putInt("exp", MainActivity.experiencia);
//            editor.apply();
//
//            if (lastExp > sp.getInt("exp", MainActivity.experiencia) || sp.getInt("exp", 33) >= 100){
//                if (im.equalsIgnoreCase("charmander")){
//                    exp.setProgress(0, true);
//                    editor.putString("who_am_i", "charmeleon");
//                    editor.putInt("exp", 0);
//                    editor.apply();
//                    im = "charmeleon";
//                    MainActivity.experiencia = 0;
//                    loadAvatar();
//                } else if(im.equalsIgnoreCase("charmeleon")){
//                    editor.putString("who_am_i", "charizard");
//                    editor.putInt("exp", 0);
//                    editor.apply();
//                    im = "charizard";
//                    MainActivity.experiencia = 0;
//                    loadAvatar();
//                }
//            }
        }
    }

    public void checkEvolution(){
        SharedPreferences sp = MainActivity.this.getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        if (shouldEvolve == true) {
            if(im.equalsIgnoreCase("charmander")){
                im = "charmeleon";
                editor.putString("who_am_i", "charmeleon");
                editor.putInt("exp", 0);
                MainActivity.experiencia = 0;
                exp.setProgress(MainActivity.experiencia, true);
            }else if(im.equalsIgnoreCase("charmeleon")){
                im = "charizard";
                editor.putString("who_am_i", "charizard");
                editor.putInt("exp", 0);
                MainActivity.experiencia = 0;
                exp.setProgress(MainActivity.experiencia, true);
            }
        }

        shouldEvolve = false;
        editor.putBoolean("should_evolve", false);
        editor.apply();

    }

    @Override
    protected void onResume() {
        checkEvolution();
        loadAvatar();
        super.onResume();
    }
}